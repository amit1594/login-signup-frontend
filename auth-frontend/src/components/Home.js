import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <h5 className="my-1" style={{ textDecoration: "underline" }}>
          Enter into Login Info System
        </h5>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          minHeight: "100vh",
        }}
      >
        <button className="btn btn-success">
          Logged In
        </button>
      </div>
    </>
  )
}

export default Home