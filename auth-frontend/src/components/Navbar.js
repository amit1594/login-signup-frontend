import React from "react";

const Navbar = () => {

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarText"
          aria-controls="navbarText"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active"></li>
          </ul>
            <span className="navbar-text mx-5">
              Welcome to the authentication system
            </span>
            <span
              className="navbar-text mx-5"
              style={{ cursor: "pointer" }}
            >
              Login-Signup Form
            </span>
            <span
              className="navbar-text mx-5"
              style={{ cursor: "pointer" }}
            >
            </span>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
