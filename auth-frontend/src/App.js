import { BrowserRouter, Routes, Route } from "react-router-dom";
import Index from "./components/Index";
import Signup from "./components/Signup";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";

function App() {
  return (
    <div className="App">
      <header className="App-header">
           <BrowserRouter>
       <ToastContainer />
       <Navbar />
       <Routes>
         <Route path="/" element={<Index />}></Route>
         <Route path="/signup" element={<Signup />}></Route>
         <Route path="/home" element={<Home />}></Route>
       </Routes>
     </BrowserRouter>
      </header>
    </div>
  );
}

export default App;


// import { BrowserRouter, Routes, Route } from "react-router-dom";
// import Index from "./components/Index";
// import Signup from "./components/Signup";
// import Navbar from "./components/Navbar";
// import "react-toastify/dist/ReactToastify.css";
// import { ToastContainer } from "react-toastify";

// const App = () => {
//   return ( 
//     <BrowserRouter>
//       <ToastContainer />
//       <Navbar />
//       <Routes>
//         <Route path="/" element={<Index />}></Route>
//         <Route path="/signup" element={<Signup />}></Route>
//       </Routes>
//     </BrowserRouter>
//   );
// }

// export default App;

